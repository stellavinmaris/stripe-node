const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const stripe = require('stripe')('sk_test_51Gr4scLWxlSWcuvAIlIeyodTrGMPYrpntatb77pDbD7CLMnlvemgDRLL3LU3gp418hXp6PVzCSG8b3OdEj8Sta9U00odez4jvo')
const responseTime = require('response-time')
const axios = require('axios');
const redis = require('redis');


const client = redis.createClient();

// Print redis errors to the console
client.on('error', (err) => {
  console.log("Error " + err);
});

// use response-time as a middleware

const app = express()

app.use(responseTime());


app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(express.static(path.join(__dirname, './views')));

app.post('/charge', (req, res) => {

    // client.set(req.body.stripeToken)
    var token = JSON.stringify(req.body.stripeToken)
    
    client.set('token', token, redis.print);

    try {
        stripe.charges.create({
            amount: req.body.amount,
            currency: 'usd',
            card: req.body.stripeToken,
            description: 'Charge card.'
        }).then(() => res.render('complete.html'))
            .catch(err => {
                console.log(err)
                res.render('error.html')

            } )
    } catch (err) { res.send(err) }
})


const port = process.env.PORT || 3000
app.listen(port, () => console.log('Server is running...'))